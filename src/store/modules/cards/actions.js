import axios from 'axios'


export const actions = {


     async CreateCard({ commit }, data) {
        const token = JSON.parse(localStorage.getItem('token'))

        const response = await axios.post('https://trello.backend.tests.nekidaem.ru/api/v1/cards/', data , {
            headers: {
                "Authorization": "JWT " + token
            }
        })
        if (response.status === 201) {
          commit('CREATE_CARD', response.data)
          return response
        }
      },

    async getCards({ commit }) {
        const token = JSON.parse(localStorage.getItem('token'))
        const response = await  axios.get(`https://trello.backend.tests.nekidaem.ru/api/v1/cards`, {
            headers: {
                "Authorization": "JWT " + token
            }
        })
        if (response.status === 200) {
            commit('GET_CARDS', response.data)
            return response
        }
      },
      
      // async refreshToken({ commit }) {
      //   const token = JSON.parse(localStorage.getItem('token'))
      //   const response = await axios.post('https://trello.backend.tests.nekidaem.ru/api/v1/users/refresh_token/', token, {
      //     token
      //   })
      //   if (response.status === 201) {
      //     commit('REFRESH_TOKEN', response.data)
      //     return response
      //   }
      // },

      async updateCardsLists({ commit }, data) {
        const token = JSON.parse(localStorage.getItem('token'))
          const id = data.id
          const response = await axios.patch(`https://trello.backend.tests.nekidaem.ru/api/v1/cards/${id}/`, { data }, {
            headers: {
                "Authorization": "JWT " + token
            }, 
          })
          if (response.status === 204) {
            commit('GET_CARDS', response.data)
            return response
          }
        },
    
      async RemoveCard({ commit }, data) {
        const token = JSON.parse(localStorage.getItem('token'))
        const id = data.id
        const response = await axios.delete(`https://trello.backend.tests.nekidaem.ru/api/v1/cards/${id}/`, {
            headers: {
                "Authorization": "JWT " + token
            }, 
            data: {}
          })
          if (response.status === 204) {
            commit('DELETE_CARD', response.data)
            return response
          }
        },
  };
