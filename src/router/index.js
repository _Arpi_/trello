import Vue from 'vue'
import Router from 'vue-router'
const Token = require('./Auth').Token

const Login = () => import('@/views/Login')
const Register = () => import('@/views/Register')

const Dashboard = () => import('@/views/Home')

Vue.use(Router)

const router = new Router({
    mode: 'history',
    linkActiveClass: 'open active!',
    scrollBehavior: () => ({ y: 0 }),
    routes: [
        {
            path: '/',
            name: 'Dashboard',
            component: Dashboard,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/register',
            name: 'Registration',
            component: Register
        },
    ]
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => to.meta.requiresAuth)) {
      if (Token.get.token()) {
        return next()
      }
      return next({ name: 'Login' })
    }
    next()
})

export default router