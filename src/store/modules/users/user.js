import axios from 'axios';
// import { APIService } from "../../APIService";
import { actions } from './actions'
import VueAxios from 'vue-axios'

// const apiService = new APIService()

const state = {
    users: [],
    user: [],
    pass: [],
    token: ''
};

const getters = {};



const mutations = {
    LOGIN: (state, user) => {
        state.user = user
        if (user.token) {
          localStorage.setItem('token', JSON.stringify(user.token))
        }
    },

    REGISTER: (state, user) => {
        state.user = user
        if (user.token) {
            localStorage.setItem('token', JSON.stringify(user.token))
          }
    },
 
 
};

export default {
    state,
    getters,
    actions,
    mutations
} 
