import Vuex from 'vuex'
import Vue from 'vue'
import users from './modules/users/user'
import cards from './modules/cards/cards'


// Load Vuex
Vue.use(Vuex);

//Create store
export default new Vuex.Store({
    modules: {
        namespaced: true,
        users,
        cards
    }
})
