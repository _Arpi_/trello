import axios from 'axios';
// import { APIService } from "../../APIService";
import { actions } from './actions'
import VueAxios from 'vue-axios'

// const apiService = new APIService()


const state = {
    cards: [],
    user: []
};

const getters = {};



const mutations = {
    CREATE_CARD: (state, cards) => {
        state.events = cards
    },

    GET_CARDS: (state, cards) => {
        state.cards = cards
    },

    // REFRESH_TOKEN: (state, user) => {
    //     state.user = user
    //     if (user.token) {
    //         localStorage.removeItem("token")    
    //         localStorage.setItem('token', JSON.stringify(user.token))
    //     }
    // },
    
    DELETE_CARD: (state, cards) => {
        state.cards = cards
    },
};

export default {
    state,
    getters,
    actions,
    mutations
} 
