export class APIService{

getToken() {
        const { token } = JSON.parse(localStorage.getItem("token"));
        return `Bearer ${token}`
    };
    getId() {
        const user = JSON.parse(localStorage.getItem("token"));
        return user.id
    }
}