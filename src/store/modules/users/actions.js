import axios from 'axios'

export const actions = {


  async Register({ commit }, data) {
    const response = await axios.post('https://trello.backend.tests.nekidaem.ru/api/v1/users/create/', data , {
      headers: {
        'Content-Type': 'application/json'
      }
    })
    if (response.status === 201) {
      commit('REGISTER', response.data)
      return response
    }
  },

  async Login({ commit }, data) {
    const response = await axios.post('https://trello.backend.tests.nekidaem.ru/api/v1/users/login/', data , {
      headers: {
        'Content-Type': 'application/json'
      }
    })
    if (response.status === 200) {
      commit('LOGIN', response.data)
      return response
    }
  },
};